package com.example.fotos_local_nuvol;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;


public class MainActivity extends AppCompatActivity{
    public static final int REQUEST_IMAGE_CAPTURE = 1;
    private String imagePath;
    private Button btn_makePhoto, btn_saveLocal, btn_saveCloud;
    private ImageView iv_image;
    private Resources res;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setVariables();
        setListeners();
    }

    private void setVariables()
    {
        btn_makePhoto = (Button) findViewById(R.id.btn_make_photo);
        btn_saveLocal = (Button) findViewById(R.id.btn_save_local);
        btn_saveCloud = (Button) findViewById(R.id.btn_save_cloud);
        iv_image = (ImageView) findViewById(R.id.iv_image);
        res = getResources();
    }

    private void setListeners()
    {
        btn_makePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkMakePhoto();
            }
        });

        btn_saveLocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dearLocal();
            }
        });

        btn_saveCloud.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dearNovol();
            }
        });
    }

    private void checkMakePhoto()
    {
        Intent intentMakePhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intentMakePhoto.putExtra( MediaStore.EXTRA_FINISH_ON_COMPLETION, true);

        if (intentMakePhoto.resolveActivity(getPackageManager()) != null) {
            File imageFile = null;
            try {
                imageFile = createImageFile();
            } catch (IOException ex) {
                createToast(this, res.getString(R.string.error_file));
                return;
            }

            if (imageFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this, "com.example.fotos_local_nuvol", imageFile);
                intentMakePhoto.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(intentMakePhoto, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            File img = new File(imagePath);
            if (img.exists()) iv_image.setImageURI(Uri.fromFile(img));
        }
    }

    private File createImageFile() throws IOException {
        String time = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fileName = "IMG_" + time + "_";
        File directory = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(fileName, ".jpg", directory);

        imagePath = image.getAbsolutePath();
        return image;
    }

    private void dearLocal() {
        Intent intentSaveLocal = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File file = new File(imagePath);
        Uri picUri = Uri.fromFile(file);
        intentSaveLocal.setData(picUri);
        this.sendBroadcast(intentSaveLocal);
        createToast(this, res.getString(R.string.correct_local));
    }

    private void dearNovol() {
        File f = new File(imagePath);
        Uri uriImage = Uri.fromFile(f);
        String idDevice = getIdDevice();
        String cloudFilePath = idDevice + uriImage.getLastPathSegment();

        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
        StorageReference storageRef = firebaseStorage.getReference();
        StorageReference uploadRef = storageRef.child(cloudFilePath);
        uploadRef.putFile(uriImage).addOnSuccessListener(taskSnapshot -> createToast(MainActivity.this, res.getString(R.string.correct_cloud)));
    }

    private String getIdDevice()
    {
        String idDevice;
        SharedPreferences preferences = this.getSharedPreferences("DEVICE_ID", Context.MODE_PRIVATE);
        idDevice = preferences.getString("DEVICE_ID", null);

        if (idDevice == null) {
            idDevice = UUID.randomUUID().toString();
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("DEVICE_ID", idDevice);
            editor.apply();
        }
        return idDevice;
    }

    private void createToast(Context context, String description)
    {
        Toast.makeText(context, description, Toast.LENGTH_SHORT).show();
    }
}
